# Test nuxt js


## Git
- each app must have a git repoository (separate steps in commits)

## 1- Setup Nuxt 2 webApp (SSR + PWA)
- create nuxt application
- nuxt app must be SSR (Server Side Rendering) + PWA (Progressive Web App)
- setup oruga & tailwind, and vee-validate
- add navbar with logo in left (@click => redirect to home page) and user icon in right
- create 4 pages: 
    -   index: Carousel of images (use static images from the internet) **use lazy loading**
    -   product page: check the [attached image](https://gitlab.com/fahdaddi/test-nuxt-js/-/raw/main/product%20page.png)  (use static data)
    -   products list: a page where all products are listed (use static data)
    -   create a login page with email & password inputs (both fields are required)
- add real time 
    -   configure and connect `pusher`
    -   add an event to delete product by id from **products list** page)

## 2- Setup Vue3 Admin
- create an admin panel with basic features [ONLY FRONTEND PART]: (login / logout / list static users / list static products)
- create unit test for login feature
